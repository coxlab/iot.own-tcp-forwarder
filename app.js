/* -*- mode:js; js-indent-level:2; -*- */
import io from 'socket.io-client';
const rp = require('request-promise');
const {PromiseSocket} = require('promise-socket');

if (process.argv.length != 7) {
  console.error('IoT.own URL, User ID and token value must be specified as parameters.');
  console.error('Usage: npm start {IoT.own URL} {user ID} {token} {destination IP} {destination port}');
  process.exit(1);
}

const server = process.argv[2];
const userId = process.argv[3];
const token = process.argv[4];
const destIp = process.argv[5];
const destPort = process.argv[6];

const socket = io(server, { forceNew: true, secure: true, reconnect: true, rejectUnauthorized: false });

socket.open();
socket.on('connect', () => {
  console.log(socket.connected);
  if (socket.connected) {
    console.log('SIO connected');
    socket.emit('joinroom', { userId: userId, token: token });
  } else {
    console.log('SIO connect failed');
    process.exit(2);
  }
});

socket.on('connect_error', (error) => {
  console.error(`SIO connect error: ${JSON.stringify(error)}`);
});

socket.on('connect_timeout', (timeout) => {
  console.error(`SIO connect timeout: ${JSON.stringify(timeout)}`);
});

socket.on('error', (error) => {
  console.error(`SIO error: ${JSON.stringify(error)}`);
});

socket.on('disconnect', (reason) => {
  console.error(`SIO disconnect: ${reason}`);
})

socket.on('data', async (nid, last_data, timestamp) => {
  console.log(`NID: ${nid}, last_data: ${JSON.stringify(last_data)}, timestamp: ${timestamp}`);

  const client = new PromiseSocket();

  if (nid.startsWith('LW') && typeof last_data.raw !== 'undefined') {
    let raw = Buffer.from(last_data.raw, 'base64');
    let nodeEui = Buffer.from(nid.substring(2), 'hex')
    let data = Buffer.concat([nodeEui, raw]);
    console.log(data);
    try {
      await client.connect(destPort, destIp);
      await client.write(Buffer.concat([nodeEui, raw]));
      await client.end();
    } catch(e) {
      console.error(e);
    }
  }
});
