FROM node:12-stretch-slim AS base

WORKDIR /root/

RUN npm install -g npm-install-missing
COPY ./package.json .

# Dependencies
FROM base AS dependencies
RUN npm set progress=false && npm config set depth 0
RUN npm install --only=production

COPY ./app.js .

CMD npm start $IOTOWN_URL $USER_ID $TOKEN $DEST
