#!/bin/sh

docker build . -f Dockerfile -t lorawan-tcp-forwarder:latest || exit 1
docker image save lorawan-tcp-forwarder:latest -o lorawan-tcp-forwarder_latest.tar || exit 1
gzip -9 -f lorawan-tcp-forwarder_latest.tar || exit 1
